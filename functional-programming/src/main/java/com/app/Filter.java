package com.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Filter {

    public static void main(String[] args) {

        List<Integer> listOfIntegers = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 8, 9));

        Predicate<Integer> isEven = (x) -> x % 2 == 0;
        List<Integer> evens = listOfIntegers.stream().filter(isEven).collect(Collectors.toList());
        System.out.println(evens);

        List<Integer> evens2 = listOfIntegers.stream().filter(x -> x % 2 == 0).collect(Collectors.toList());
        System.out.println(evens2);


        List<String> wordArray = new ArrayList<>(Arrays.asList("hello", "functional", "programming", "java", "cool"));
        Function<Integer, Predicate<String>> createLength = (minLength) ->{
            return (str) -> str.length()> minLength;
        };

        Predicate<String> isLongerThan5 = createLength.apply(5);

        List<String> longWords = wordArray.stream().filter(isLongerThan5).collect(Collectors.toList());
        System.out.println(longWords);
    }
}
