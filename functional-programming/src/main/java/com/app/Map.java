package com.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Map {

    public static void main(String[] args) {

        List<Integer> listOfIntegers = new ArrayList<>(Arrays.asList(3, 4, 5, 6, 8, 9));

        Function<Integer, Integer> timesTwo =(x)-> x*2;
        List<Integer> doubled = listOfIntegers.stream().map(timesTwo).collect(Collectors.toList());
        System.out.println(doubled);

        List<Integer> doubled2 = listOfIntegers.stream().map((x)-> x*2).collect(Collectors.toList());
        System.out.println(doubled2);


    }
}
