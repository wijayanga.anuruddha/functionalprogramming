package com.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Collect {

    public static void main(String[] args) {

        List<String> wordArray = new ArrayList<>(Arrays.asList("hello", "functional", "programming", "java", "cool"));

        String longWord = wordArray.stream().filter((word)->word.length()>4).collect(Collectors.joining(", "));
        System.out.println(longWord);

        Long longWordCount = wordArray.stream().filter((word)->word.length()>4).collect(Collectors.counting());
        System.out.println(longWordCount);

        Map<Integer, List<String>> wordLengthMap = wordArray.stream().collect(Collectors.groupingBy(
                (word) ->word.length() 
        ));
        System.out.println(wordLengthMap);

        Map<Boolean, List<String>> wordLengthMap2 = wordArray.stream().collect(Collectors.partitioningBy(
                (word) ->word.length() >5
        ));
        System.out.println(wordLengthMap2);
    }
}
