package com.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Parallel {

    public static void main(String[] args) {

        List<String> wordArray = new ArrayList<>(Arrays.asList("hello", "functional", "programming", "java", "cool"));

        List<String> processedWords = wordArray
                .parallelStream()
                .map((word) -> {
                    System.out.println("uppercase ".concat(word));
                    return word.toUpperCase();
                })
                .map((word) -> {
                    System.out.println("adding exclamation point ".concat(word));
                    return word.concat("!");
                })
                .collect(Collectors.toList());

        System.out.println(processedWords);
    }
}
