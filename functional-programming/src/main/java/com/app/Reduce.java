package com.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

public class Reduce {

    public static void main(String[] args) {

        List<Integer> listOfInteger = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9));

        BinaryOperator<Integer> getSum = (acc, x) ->{
            return acc+x;
        };

        Integer sum = listOfInteger.stream().reduce(0, getSum);
        System.out.println(sum);
    }
}
